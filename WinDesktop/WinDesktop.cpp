﻿// WindowsProject1.cpp : Определяет точку входа для приложения.
//
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <WinGDI.h>
#include "framework.h"
#include "WinDesktop.h"
#pragma comment(lib, "Msimg32.lib")

static TCHAR szWindowClass[] = _T("DesktopApp");

// The string that appears in the application's title bar.
static TCHAR szTitle[] = _T("Windows Desktop Guided Tour Application");

// Stored instance handle for use in Win32 API calls such as FindResource
HINSTANCE hInst;

// Forward declarations of functions included in this code module:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(
    _In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPSTR     lpCmdLine,
    _In_ int       nCmdShow
)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(wcex.hInstance, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(CreateSolidBrush(RGB(200, 152, 204))); //изменение фона окна
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

    if (!RegisterClassEx(&wcex))
    {
        MessageBox(NULL,
            _T("Call to RegisterClassEx failed!"),
            _T("Windows Desktop Guided Tour"),
            NULL);

        return 1;
    }

    // Store instance handle in our global variable
    hInst = hInstance;

    // The parameters to CreateWindowEx explained:
    // WS_EX_OVERLAPPEDWINDOW : An optional extended window style.
    // szWindowClass: the name of the application
    // szTitle: the text that appears in the title bar
    // WS_OVERLAPPEDWINDOW: the type of window to create
    // CW_USEDEFAULT, CW_USEDEFAULT: initial position (x, y)
    // 500, 100: initial size (width, length)
    // NULL: the parent of this window
    // NULL: this application does not have a menu bar
    // hInstance: the first parameter from WinMain
    // NULL: not used in this application
    HWND hWnd = CreateWindowEx(
        WS_EX_OVERLAPPEDWINDOW,
        szWindowClass,
        szTitle,
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        900, 900,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    if (!hWnd)
    {
        MessageBox(NULL,
            _T("Call to CreateWindow failed!"),
            _T("Windows Desktop Guided Tour"),
            NULL);

        return 1;
    }

    // The parameters to ShowWindow explained:
    // hWnd: the value returned from CreateWindow
    // nCmdShow: the fourth parameter from WinMain
    ShowWindow(hWnd,
        nCmdShow);
    UpdateWindow(hWnd);

    // Main message loop:
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;
    static int x, y;
    switch (message)
    {
    case WM_GETMINMAXINFO: 

    {
        MINMAXINFO* pInfo = (MINMAXINFO*)lParam;
        POINT Min = { 100, 100 }; //минимальный размер окна (меньше картинки 450*450)
        POINT  Max = { 900, 900 }; //максимальный размер окна
        pInfo->ptMinTrackSize = Min; // Установили минимальный размер
        pInfo->ptMaxTrackSize = Max; // Установили максимальный размер
        return 0;

    }

    case WM_SIZE: //получение размера окна
    {
        x = LOWORD(lParam); //получает ширину
        y = HIWORD(lParam); //получает высоту
        break;
    }

    case WM_PAINT: //опредление положения картинки и её загрузка в проект
        hdc = BeginPaint(hWnd, &ps);
        {                                                           //загрузка рисунка (он находится в директории проекта)
            HBITMAP hBitmap = (HBITMAP)LoadImage(NULL, L"..//WinDesktop/picture.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE); 
            BITMAP Bitmap; 
            GetObject(hBitmap, sizeof(BITMAP), &Bitmap);
            HDC hCompatibleDC = CreateCompatibleDC(hdc);
            HBITMAP hOldBitmap = (HBITMAP)SelectObject(hCompatibleDC, hBitmap);
            //отрисовываем изображение в заданных координатах, нужного размера, а так же убирем её задний фн
            TransparentBlt(hdc, (x - 450) / 2, (y - 450)/2, 450, 450, hCompatibleDC, 0, 0, 450, 450, RGB(63, 72, 204));
            SelectObject(hCompatibleDC, hOldBitmap);
            DeleteObject(hBitmap);
            DeleteDC(hCompatibleDC);
        }
        EndPaint(hWnd, &ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
        break;
    }
    return 0;
}